/*
 * config.h
 *
 *  Created on: May 13, 2011
 *      Author: glpuga
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define VERSION			0.1

#define DEFAULT_ADDR    "127.0.0.1"
#define DEFAULT_PORT	23000


#endif /* CONFIG_H_ */
