/*
 * Serial to TCP bridge
 * Copyright (C) 2011  Gerardo Puga, glpuga@gmail.com
 *
 * Based on remserial 1.4  */

/* remserial 1.4
 * Copyright (C) 2000  Paul Davis, pdavis@lpccomp.bc.ca
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * This program acts as a bridge either between a socket(2) and a
 * serial/parallel port or between a socket and a pseudo-tty.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <termios.h>
#include "defaults.h"

#define SERIAL2TCP_VER "0.2"

int curConnects = 0;
int *remotefd;
int devfd;
int sockfd = -1;
fd_set fdsread,fdsreaduse;

static struct sttyset {
	int speed;
	int termiosvalue;
} termiosSpeedValues[] = {
		{ 50,		B50	},
		{ 75,		B75	},
		{ 110,		B110	},
		{ 134,		B134	},
		{ 150,		B150	},
		{ 200,		B200	},
		{ 300,		B300	},
		{ 600,		B600	},
		{ 1200,		B1200	},
		{ 1800,		B1800	},
		{ 2400,		B2400	},
		{ 4800,		B4800	},
		{ 9600,		B9600	},
		{ 19200,	B19200	},
		{ 38400,	B38400	},
#ifdef B57600
		{ 57600,	B57600	},
#endif
#ifdef B115200
		{ 115200,	B115200	},
#endif
#ifdef B230400
		{ 230400,	B230400	},
#endif
		{ 0,	B0	} /* end of list indicator, don't erase */
};

void sighandler(int sig)
{
	int i;

	if ( sockfd != -1 )
	{
		close(sockfd);
	}

	for (i=0 ; i<curConnects ; i++)
	{
		close(remotefd[i]);
	}

	if ( devfd != -1 )
	{
		close(devfd);
	}

	syslog(LOG_ERR,"Terminating on signal %d",sig);
	exit(0);
}

void heading()
{
	printf("\n");
	printf("Serial to TCP bridge %s. Compilation date: %s\n", SERIAL2TCP_VER, __DATE__);
	printf("Gerardo Puga, 07/2011.\n");
	printf("\n");
}

void usage(char *progname) {
	heading();
	printf("Based on Remserial version 1.4.\n");
	printf("Usage information:\n\n");
	printf("  %s [-b] [-d] [-p netport] [-s speed] [-m maxconn] device\n\n", progname);

	printf("  -b            Enable broadcasting of each client's uplink channel to the downlink channels\n");
	printf("                of the rest of the clients. Use this feature with care, as it will cause\n");
	printf("                broken messages and other reception errors.\n");
	printf("  -d            Run as a system daemon.\n");
	printf("  -m maxconn    Maximum number of simultaneous client connections to accept (default 20).\n");
	printf("  -p netport    TCP port number to listen for connections (default 23000)\n");
	printf("  -s speed      Baudrate of the serial port.\n");
	printf("  -x dbglev     Set debug level (0, 1, 2, 3).\n");
	printf("  device        Serial port device file.\n");
	printf("\n");
}

void configPort(int sdevname, int termiosPortSpeed)
{
	struct termios term;

	if (tcgetattr (sdevname, &term) >= 0)
	{
		/* make a raw terminal, Linux MANuals */
		term.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
		term.c_oflag &= ~OPOST;
		term.c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
		term.c_cflag &= ~(CSIZE | PARENB);
		term.c_cflag |= (CS8 | CLOCAL| CREAD);

		term.c_cc[VMIN]  = 1;
		term.c_cc[VTIME] = 0;

		cfsetispeed(&term, termiosPortSpeed);
		cfsetospeed(&term, termiosPortSpeed);

		tcsetattr (sdevname, TCSADRAIN, &term);
	}
}

int main(int argc, char *argv[])
{
	struct sockaddr_in addr,remoteaddr;
	int port;
	static char *sdevname = NULL;


	struct hostent *remotehost;
	int result;
	extern char *optarg;
	extern int optind;
	int maxfd = -1;
	char devbuf[512];
	int devbytes;
	int remoteaddrlen;
	int reuseaddr;
	int childPid;
	int c;

	int maxConnects;
	int debug;
	int isdaemon;
	int clientDataRetransmissionEnabled;

	int portSpeed, termiosPortSpeed;

	register int i, o;

	/* run configuration default values */
	port              = DEFAULT_PORT;
	maxConnects       = 20;
	portSpeed         = 38400;
	termiosPortSpeed  = B38400;
	debug             = 2;
	isdaemon          = 0;
	clientDataRetransmissionEnabled = 0;

	while ( (c=getopt(argc,argv,"dbm:p:s:x:")) != EOF )
	{
		switch (c) {
		case 'd':
			isdaemon = 1;
			break;
		case 'b':
			clientDataRetransmissionEnabled = 1;
			break;
		case 'x':
			debug = atoi(optarg);
			break;
		case 'm':
			maxConnects = atoi(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 's':
			portSpeed = atoi(optarg);
			termiosPortSpeed = B0;
			for (i = 0 ; termiosSpeedValues[i].speed; i++)
			{
				if (portSpeed == termiosSpeedValues[i].speed)
				{
					/* valid speed value */
					termiosPortSpeed = termiosSpeedValues[i].termiosvalue;
				}
			}
			if (termiosPortSpeed == B0)
			{
				printf("Invalid speed setting!\n");
				usage(argv[0]);
				exit(1);
			}
			break;
		case '?':
			usage(argv[0]);
			exit(1);
			break;
		default:
			printf("Unknown parameter!!!\n");
			usage(argv[0]);
			exit(1);
		}
	}

	/* any remaining argument must be the name of the serial device */
	sdevname = argv[optind];
	remotefd = (int *) malloc (maxConnects * sizeof(int));

	if (!sdevname)
	{
		usage(argv[0]);
		exit(1);
	}

	/* Open a syslog log. This is useful when the program runs as a daemon. */
	openlog("serial2tcp", LOG_PID, LOG_USER);

	heading();
	printf("  TCP port number : %d\n", port);
	printf("  Max TCP clients : %d\n", maxConnects);
	printf("Serial Port Speed : %d\n", portSpeed);
	printf("      Debug Level : %d\n", debug);
	printf("      Daemon Mode : %s\n", isdaemon?"on":"off");
	printf("   Retransmission : %s\n", clientDataRetransmissionEnabled?"on":"off");
	printf("\n");
	printf("Activity:\n");

	/* Send the same information to the system log */
	if (debug > 1)
	{
		syslog(LOG_NOTICE,"  TCP port number : %d\n", port);
		syslog(LOG_NOTICE,"  Max TCP clients : %d\n", maxConnects);
		syslog(LOG_NOTICE,"Serial Port Speed : %d\n", portSpeed);
		syslog(LOG_NOTICE,"      Debug Level : %d\n", debug);
		syslog(LOG_NOTICE,"      Daemon Mode : %s\n", isdaemon?"on":"off");
		syslog(LOG_NOTICE,"   Retransmission : %s\n", clientDataRetransmissionEnabled?"on":"off");
	}

	devfd = open(sdevname,O_RDWR);
	if ( devfd == -1 ) 
	{
		syslog(LOG_ERR, "Open of %s failed: %m",sdevname);
		printf("\n***ERROR: Can't open %s: %m\n\n", sdevname);
		exit(1);
	}

	configPort(devfd, termiosPortSpeed);

	signal(SIGINT,sighandler);
	signal(SIGHUP,sighandler);
	signal(SIGTERM,sighandler);

	/* We are the server */

	/* Open the initial socket for communications */
	sockfd = socket(AF_INET, SOCK_STREAM, 6);
	if ( sockfd == -1 ) {
		syslog(LOG_ERR, "Can't open socket: %m");
		printf("\n***ERROR: Can't open socket: %m\n\n");
		exit(1);
	}

	/* Configure the service socket so that it can bind even if the port is currently in TIME_WAIT.
	 * Otherwise it is very annoying to have to wait for the TIME_WAIT period to expire. */
	reuseaddr = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) < 0)
	{
		syslog(LOG_ERR, "Could not set the SO_REUSEADDR option to the socket: %m");
		printf("\n***ERROR: Can't open socket: %m\n\n");
		exit(1);
	}

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = 0;
	addr.sin_port = htons(port);

	/* Set up to listen on the given port */
	if(bind( sockfd, (struct sockaddr*)(&addr), sizeof(struct sockaddr_in)) < 0)
	{
		syslog(LOG_ERR, "Couldn't bind port %d, aborting: %m",port );
		printf("\n***ERROR: Couldn't bind port %d, is there another server session alredy running?\n\n",port );
		exit(1);
	}

	printf("Bound port\n");

	if (debug > 1)
	{
		syslog(LOG_NOTICE,"Bound port");
	}

	/* Tell the system we want to listen on this socket */
	result = listen(sockfd, 4);
	if (result == -1)
	{
		syslog(LOG_ERR, "Socket listen failed: %m");
		printf("\n***ERROR: Socket listen failed: %m\n\n");
		exit(1);
	}

	printf("Done listen\n");

	if (debug > 1)
	{
		syslog(LOG_NOTICE,"Done listen");
	}

	if (isdaemon)
	{
		printf("Going into background\n");

		/* Perform a fork and termina if you are the parent */
		if ((childPid = fork()) > 0)
		{
			printf("Child forked, parent ending. Follow child on SYSLOG\n\n");
			printf("Childe PID: %d\n", childPid);

			exit(0);
		}

		setsid();
		chdir("/");
		umask(0);

		close(0);
		close(1);
		close(2);

		syslog(LOG_NOTICE, "I'm the child! My PID is %d\n", getpid());
	}

	/* Set up the files/sockets for the select() call */
	if (sockfd != -1)
	{
		FD_SET(sockfd,&fdsread);

		if ( sockfd >= maxfd )
		{
			maxfd = sockfd + 1;
		}
	}

	if (!daemon)
	{
		printf("Sleeping on select\n");
	}

	if (debug > 1)
	{
		syslog(LOG_NOTICE,"Sleeping on select");
	}

	FD_SET(devfd,&fdsread);

	if (devfd >= maxfd)
	{
		maxfd = devfd + 1;
	}

	while (1)
	{
		/* Wait for data from the listening socket, the device
		   or the remote connection */
		fdsreaduse = fdsread;

		if (select(maxfd,&fdsreaduse,NULL,NULL,NULL) == -1)
		{
			if (debug > 0)
			{
				syslog(LOG_ERR,"select error: %m");
			}
			break;
		}

		/* Activity on the controlling socket, only on server */
		if (FD_ISSET(sockfd,&fdsreaduse))
		{
			int fd;

			/* Accept the remote systems attachment */
			remoteaddrlen = sizeof(struct sockaddr_in);
			fd = accept(sockfd, (struct sockaddr*)(&remoteaddr),	&remoteaddrlen);

			if (fd == -1)
			{
				syslog(LOG_ERR,"accept failed: %m");

			} else {

				if (curConnects < maxConnects)
				{
					unsigned long ip;

					remotefd[curConnects++] = fd;

					/* Tell select to watch this new socket */
					FD_SET(fd,&fdsread);

					if (fd >= maxfd)
					{
						maxfd = fd + 1;
					}

					ip = ntohl(remoteaddr.sin_addr.s_addr);

					syslog(LOG_NOTICE, "Connection from %d.%d.%d.%d", (int)(ip>>24)&0xff, (int)(ip>>16)&0xff, (int)(ip>>8)&0xff, (int)(ip>>0)&0xff);

				} else {
					/* Too many connections, just close it to reject */
					syslog(LOG_ERR,"A connection as been refused. Increase the maximum number of connections with \"-m\"");

					close(fd);
				}
			}
		}

		/* Data to read from the device */
		if (FD_ISSET(devfd,&fdsreaduse))
		{
			devbytes = read(devfd,devbuf,512);

			if (debug > 3)
			{
				syslog(LOG_INFO,"Device data: %d bytes, devfd %d",devbytes, devfd);
			}

			if (devbytes <= 0)
			{
				if (debug > 0)
				{
					syslog(LOG_INFO,"%s closed",sdevname);
				}

				close(devfd);
				FD_CLR(devfd,&fdsread);

				while (1)
				{
					devfd = open(sdevname,O_RDWR);
					if (devfd != -1)
					{
						break;
					}

					syslog(LOG_ERR, "Open of %s failed: %m", sdevname);

					if (errno != EIO)
					{
						exit(1);
					}

					sleep(1);
				}

				configPort(devfd, termiosPortSpeed);

				if (debug > 0)
				{
					syslog(LOG_INFO,"%s re-opened",sdevname);
				}

				FD_SET(devfd,&fdsread);

				if (devfd >= maxfd)
				{
					maxfd = devfd + 1;
				}

			} else {

				if (debug > 3)
				{
					syslog(LOG_INFO,"Writing data to device...");
				}

				for (i = 0 ; i < curConnects ; i++)
				{
					/* MSG_NOSIGNAL es para que no se genere una señal SIGPIPE si trato de escribir
					 * en una conexión que fue cerrada pero que todavía no fue quitada de la lista
					 * de conexiones (porque se cerró simultáneamente con la recepción de este bloque
					 * desde otro peer). */
					if (send(remotefd[i], devbuf, devbytes, MSG_NOSIGNAL) == -1)
					{
						if (debug > 0)
						{
							syslog(LOG_ERR,"device to client write error: %m (i = %d, curConnects = %d)", i, curConnects);
						}
					}
				}

				if (debug > 3)
				{
					syslog(LOG_INFO,"End of input processing");
				}
			}
		}

		/* Data to read from the remote system */
		for (i = 0 ; i < curConnects ; i++)
		{
			if (FD_ISSET(remotefd[i],&fdsreaduse))
			{
				devbytes = read(remotefd[i],devbuf,512);

				if (debug > 3)
				{
					syslog(LOG_INFO,"Remote data: %d bytes, fd %d",devbytes, remotefd[i]);
				}

				if (devbytes == 0)
				{
					register int j;

					syslog(LOG_NOTICE,"Connection closed");
					close(remotefd[i]);

					FD_CLR(remotefd[i],&fdsread);
					curConnects--;
					for (j=i ; j < curConnects ; j++)
					{
						remotefd[j] = remotefd[j+1];
					}

				} else {
					/* Write the data to the device */
					if (debug > 3)
					{
						syslog(LOG_INFO,"Writing data to device:");
					}

					if (devfd != -1)
					{
						if (write(devfd,devbuf,devbytes) == -1)
						{
							if (debug > 0)
							{
								syslog(LOG_ERR,"client to device write error: %m (devfd = %d, i = %d, curConnects = %d)", devfd, i, curConnects);
							}
						}
					}

					if (debug > 3)
					{
						syslog(LOG_INFO,"Writing data to other clients...");
					}

					if (clientDataRetransmissionEnabled == 1)
					{
						/* write data to the other devices */
						for (o = 0 ; o < curConnects ; o++)
						{
							if (remotefd[i] != remotefd[o])
							{
								/* MSG_NOSIGNAL es para que no se genere una señal SIGPIPE si trato de escribir
								 * en una conexión que fue cerrada pero que todavía no fue quitada de la lista
								 * de conexiones (porque se cerró simultáneamente con la recepción de este bloque
								 * desde otro peer). */
								if (send(remotefd[o], devbuf, devbytes, MSG_NOSIGNAL) == -1)
								{
									if (debug > 0)
									{
										syslog(LOG_ERR,"client to client write error: %m (o = %d, i = %d, curConnects = %d)", o, i, curConnects);
									}
								}
							}
						}
					}

					if (debug > 3)
					{
						syslog(LOG_INFO,"End of input processing");
					}
				}
			}
		}
	}

	syslog(LOG_ERR,"Closing connections...");

	close(sockfd);

	for (i=0 ; i<curConnects ; i++)
	{
		close(remotefd[i]);
	}

	syslog(LOG_ERR,"Terminating.");

	return 0;
}


